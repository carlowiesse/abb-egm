# ABB EGM (Externally Guided Motion)

## Contents

Current `egm.proto` file is from `RobotWare 7.12.0`

## Where do I find my own `egm.proto` file?

```shell
C:\Users\<user>\AppData\Local\ABB\RobotWare\RobotControl_X.X.X\utility\Template\EGM
```

where `X.X.X` is your `RobotWare` version

## How do I generate my own `egm_pb2.py` file?

Use the following online tool: [Protobuf Code Generator and Parser](https://protogen.marcgravell.com/)

This tool can be used to generate a `my_pb2.py` file from any `.proto` file.
