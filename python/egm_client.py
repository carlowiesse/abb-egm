import socket

from egm_pb2 import EgmRobot, EgmSensor, EgmHeader


def create_egm_sensor_message(n, x, y, z, rx, ry, rz):
    msg = EgmSensor()
    msg.header.mtype = EgmHeader.MessageType.Value('MSGTYPE_CORRECTION')
    msg.header.seqno = n
    msg.planned.cartesian.pos.x = x
    msg.planned.cartesian.pos.y = y
    msg.planned.cartesian.pos.z = z
    msg.planned.cartesian.euler.x = rx
    msg.planned.cartesian.euler.y = ry
    msg.planned.cartesian.euler.z = rz
    return msg

def check_current_pose(msg1, msg2, threshold=0.1):
    dx = abs(msg1.feedBack.cartesian.pos.x - msg2.planned.cartesian.pos.x)
    dy = abs(msg1.feedBack.cartesian.pos.y - msg2.planned.cartesian.pos.y)
    dz = abs(msg1.feedBack.cartesian.pos.z - msg2.planned.cartesian.pos.z)
    drx = abs(msg1.feedBack.cartesian.euler.x - msg2.planned.cartesian.euler.x) % 360
    dry = abs(msg1.feedBack.cartesian.euler.y - msg2.planned.cartesian.euler.y) % 360
    drz = abs(msg1.feedBack.cartesian.euler.z - msg2.planned.cartesian.euler.z) % 360
    deltas = [dx, dy, dz, drx, dry, drz]
    return max(deltas) < threshold

def main():
    # As defined in Controller > Configuration > Communication > UDP Unicast Device
    #remote_addr = '192.168.131.5' # For physical robot connected via MGMT port
    #remote_addr = '192.168.125.5' # For physical robot connected via WAN port
    remote_addr = '127.0.0.1' # For virtual robot simulated in robotstudio
    remote_port = 6511

    # Set up client to receive UDP messages
    remote_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind client to listen on selected IP and port
    remote_socket.bind((remote_addr, remote_port))

    print(f'Listening on {remote_addr}:{remote_port}')

    # Pose defined as (x, y, z, rx, ry, rz) where
    # (x, y, z) are the cartesian coordinates in mm
    # and (rx, ry, rz) are the euler angles in degrees
    poses = [
        (600, 0, 800, 180, 60, 180),
        (600, 0, 700, 180, 60, 180),
        (600, 0, 900, 180, 60, 180),
    ]

    for i, pose in enumerate(poses):
        while True:
            data, addr = remote_socket.recvfrom(1024)  # Buffer size is 1024 bytes
            
            # Read and deserialize message from robot controller
            egm_robot_msg = EgmRobot()
            egm_robot_msg.ParseFromString(data)

            # Setup message for sending to robot controller
            egm_sensor_msg = create_egm_sensor_message(i, *pose)

            # Check if current pose is the same as target pose
            if check_current_pose(egm_robot_msg, egm_sensor_msg):
                print('---')
                print(f'Reached pose {pose}')
                print('---')
                print(egm_robot_msg.feedBack)
                print(f'Message from {addr[0]}:{addr[1]}')
                break
            
            # Serialize and send message to robot controller
            msg = egm_sensor_msg.SerializeToString()
            remote_socket.sendto(msg, addr)
        

if __name__ == '__main__':
    main()
